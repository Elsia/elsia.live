# elsia.live

Yeah, well, personal portfolio. It's open source because it's so beautiful, it would be a shame not to, right?

## To do
- [ ] Important and urgent tasks
    - [ ] Upload the proprietary pictures into /res
    - [ ] Change these pictures' paths in the html files
- [ ] Prepare scaling
    - [ ] Transition to pug
    - [ ] Clean up the source code and ~ beautify ~
    - [ ] Create several pages
    - [ ] Lighten the landing page
    - [ ] Work on back-end development
    - [ ] Create a private media hosting app
- [ ] Complete content
    - [x] Profile picture
    - [x] Bio
    - [x] Projects
    - [x] Social networks links
    - [ ] Legal mentions
    - [ ] Blog
- [ ] Design improvements
    - [ ] Responsive #navbar
    - [ ] #navbar links on the blocks, not on the text
    - [x] Project links on the projects-img
    - [ ] Projects links on "more" button
    - [ ] Add a scroll hint
    - [ ] Maybe some more desktop animations
- [ ] Improve the #contact section
    - [ ] Transfer svg into separate files
    - [ ] W3C compliance with #contact section headings
- [ ] Deployment
    - [x] Add a free licence to the source code in /src
    - [x] Add a private licence on resources in /res
    - [x] Create an open source repository on git.bitmycode.com
    - [x] Import the project into the repository
    - [x] Deploy on the bitmycode server
    - [x] Redirect https://elsia.live to the server's public IP
    - [ ] Acquire a personal server :p
    - [ ] Self-host